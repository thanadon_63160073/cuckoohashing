/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.cuckoo;

/**
 *
 * @author informatics
 */
public class CuckooTable {
    String value;
    int key;

    public CuckooTable() {
        value = "-";
        key = Integer.MIN_VALUE;
    }

    public String getValue() {
        return value;
    }

    public int getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(int key) {
        this.key = key;
    }
    
}
