/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.cuckoo;

import java.util.Scanner;
/**
 *
 * @author Acer
 */
public class testCuckoo {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        CuckooHashing.fillTable();
        while(true){
            CuckooHashing.printTable();
            showMenu();
            String menu = kb.next();
            switch (menu) {
                case "add":
                    {
                        String value = inputValue(kb);
                        int key = inputKey(kb);
                        CuckooHashing.add(value, key);
                        break;
                    }
                case "delete":
                    {
                        int key = inputKey(kb);
                        CuckooHashing.delete(key);
                        break;
                    }
                case "lookup":
                    {
                        int key = inputKey(kb);
                        System.out.println(CuckooHashing.lookup(key));
                        break;
                    }
                case "exit":
                    return;
                default:
                    {
                        System.out.println("Error function is not correct!!!!!!");
                        break;
                    }   
            }
            System.out.println("========================================");
        }       
    }

    private static int inputKey(Scanner kb) {
        System.out.println("Enter key:");
        int key = kb.nextInt();
        return key;
    }

    private static String inputValue(Scanner kb) {
        System.out.println("Enter value:");
        String value = kb.next();
        return value;
    }

    private static void showMenu() {
        System.out.println("Choose menu function:");
        System.out.println("1.add");
        System.out.println("2.delete");
        System.out.println("3.lookup");
        System.out.println("4.exit");
    }
}
