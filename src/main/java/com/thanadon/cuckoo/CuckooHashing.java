/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.cuckoo;

/**
 *
 * @author informatics
 */
public class CuckooHashing {

    private static int MOD = 11;
    private static int nest = 2;
    private static CuckooTable[][] cuckooTable = new CuckooTable[nest][MOD];
    private static int[] POSITION = new int[nest];
    private static int valueLength = 1;

    public CuckooHashing() {
    }

    public static void fillTable() {   //ใช้สำหรับเติม cuckooTable ไว้เช็คว่ามี key ถูกเติมในตำแหน่งนั้นหรือยัง
        for (int j = 0; j < MOD; j++) {
            for (int i = 0; i < nest; i++) {
                cuckooTable[i][j] = new CuckooTable();
            }
        }
    }

    public static int getHashValue(int function, int key) { //ใช้สำหรับหาค่าของตำแหน่งที่ key จะได้อยู่ทั้งสองตาราง
        switch (function) {
            case 1:
                return key % MOD;
            case 2:
                return (key / MOD) % MOD;
        }
        return Integer.MIN_VALUE;
    }

    public static void getArrange(String value, int key, int id, int c, int n) { //ใช้สำหรับจัดตำแหน่งที่ key และ value จะได้อยู่ตารางไหน ตำแหน่่งไหน
        if (c == n) { //กรณีที่ไม่สามารถหาตำแหน่งที่จะอยู่ได้ จะไม่ถูกนำมาเก็บ
            System.out.println(key + " is not  have a position");
            return;
        }
        for (int i = 0; i < nest; i++) { //เก็บตำแหน่งของ key ทั้งสองตารางไว้
            POSITION[i] = getHashValue(i + 1, key);
            if (cuckooTable[i][POSITION[i]].getKey() == key) {
                return;
            }
        }
        if (cuckooTable[id][POSITION[id]].getKey() != Integer.MIN_VALUE) { //ถ้าตำแหน่งดังกล่าวมี key อยู่แล้วให้ key ใหม่แทนที่ key เก่า และหาตำแหน่งใหม่ให้ key เก่าอยู่
            int dis = cuckooTable[id][POSITION[id]].getKey();
            String oldValue = cuckooTable[id][POSITION[id]].getValue();
            cuckooTable[id][POSITION[id]].setKey(key);
            cuckooTable[id][POSITION[id]].setValue(value);
            getArrange(oldValue, dis, (id + 1) % nest, c + 1, n);
            valueLength++;
        } else { //ถ้าตำแหน่งนั้นไม่มี key ให้ใส่ value และ key ที่ตำแหน่งนั้นได้เลย
            cuckooTable[id][POSITION[id]].setKey(key);
            cuckooTable[id][POSITION[id]].setValue(value);
            valueLength++;
        }
    }

    public static void printTable() { //แสดงข้อมูลในตาราง value
        System.out.println("Hash Tables are:");
        for (int i = 0; i < nest; i++, System.out.println()) {
            int t = i + 1;
            System.out.print(" Table: " + t + " --> ");
            for (int j = 0; j < MOD; j++) {
                System.out.print("  " + cuckooTable[i][j].getValue());
            }
            System.out.println();
        }
    }
    
    public static void add(String value, int key) { //เพิ่มข้อมูล
        for (int i = 0, cnt = 0; i < valueLength; i++, cnt = 0) {
            getArrange(value, key, 0, cnt, valueLength);
        }
    }

    public static void delete(int key) { //ลบข้อมูลด้วย key
        for (int i = 0; i < nest; i++) {
            POSITION[i] = getHashValue(i + 1, key);
            if (cuckooTable[i][POSITION[i]].getKey() == key) {
                cuckooTable[i][POSITION[i]].setKey(Integer.MIN_VALUE);
                cuckooTable[i][POSITION[i]].setValue("-");
                return;
            }
        }
    }

    public static String lookup(int key) { //ค้นหาข้อมูลด้วย key
        for (int i = 0; i < nest; i++) {
            POSITION[i] = getHashValue(i + 1, key);
            if (cuckooTable[i][POSITION[i]].getKey() == key) {
                return "Value: " + cuckooTable[i][POSITION[i]].getValue();
            }
        }
        return key+" is not found value";
    }
}
